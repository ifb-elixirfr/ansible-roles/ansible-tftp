This role is a customized version of bertvv.tftp https://github.com/bertvv/ansible-role-tftp

'Variables'
The variables are either in group_vars/ or in defaults/


Find below the README for the original role, with some modifications to account for the customization.

# Ansible role `tftp`

An Ansible role for installing a TFTP (Trivial File Transfer Protocol) server on RHEL/CentOS 7. Specifically, the responsibilities of this role are to:

- install the necessary packages
- manage the configuration

For more relevant documentation on TFTP, see:

- [`tftpd(8)` man page](http://linuxmanpages.net/manpages/fedora21/man8/tftpd.8.html)
- [`tftp_selinux(8)` man page](http://linuxmanpages.net/manpages/fedora21/man8/tftpd_selinux.8.html)


## Role Variables

The following variables can be set by the administrator:

| Variable                      | Default             | Comments (type)                                                      |
| :---                          | :---                | :---                                                                 |
| `tftp_root_directory`         | `/var/lib/tftpboot` | The path to the root directory served by TFTP.                       |
| `tftp_server_args`            | `--secure`          | Arguments to be passed to the server (except root directory)         |
|                               |                     | See the `tftpd` man page for details                                 |
| `tftp_server_foreman_support` | false               | Enable Foreman support by creating suitable tftpd.map                |
| `tftp_setype`                 | `tftpdir_rw_t`      | Default SELinux context for the root directory.                      |
|                               |                     | Set to `public_content_rw_t` to allow access through other services  |
| `tftp_anon_write`             | no                  | When set to "yes", `tftp` can modify public files.                   |
| `tftp_home_dir`               | no                  | When set to "yes", `tftp` can modify files in user home directories. |

The following variables usually should be left alone:

| Variable           | Default              | Comments (type)                           |
| :---               | :---                 | :---                                      |
| `tftp_cps`         | 100 2                |                                           |
| `tftp_disable`     | no                   | When set to 'yes', TFTP will be disabled. |
| `tftp_flags`       | IPv4                 |                                           |
| `tftp_group`       | root                 | Group of `tftp_root_directory`            |
| `tftp_mode`        | 0755                 | Permissions of `tftp_root_directory`      |
| `tftp_per_source`  | 11                   |                                           |
| `tftp_protocol`    | udp                  |                                           |
| `tftp_server`      | `/usr/sbin/in.tftpd` |                                           |
| `tftp_socket_type` | dgram                |                                           |
| `tftp_user`        | root                 |                                           |
| `tftp_wait`        | yes                  |                                           |

## Dependencies

No dependencies.


## License

BSD

## Author Information

Bert Van Vreckem (bert.vanvreckem@gmail.com)

Contributions by:

- [@kostyrevaa ](https://github.com/kostyrevaa): Foreman support (v1.2.0)
